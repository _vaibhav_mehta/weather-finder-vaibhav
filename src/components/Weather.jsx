import React from 'react';
const weather=(props)=>{
    return (
        <div className="container pt-5 text-light">
            <div className="cards">
                <h2>
                        {props.city}
                </h2>
                <h5 className="py-4">
                    <i className={`wi ${props.weatherIcon} display-1`}></i>
                </h5>
               {props.temp_celcius?<h2 className = "py-2"> {props.temp_celcius}&deg;</h2>:null}
                
                {min_max(props.temp_min,props.temp_max)}
                <h4 className="py-3">{props.description} </h4>
         </div>
        </div>
    );
}
function min_max(min,max){
  if(min &&max){
    return (
        <h3>
            <span className="px-4">{min}&deg;</span>
            <span className="px-4">{max}&deg;</span>
        </h3>
    );
  }
}
export default weather;