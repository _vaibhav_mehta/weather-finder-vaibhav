import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import "weather-icons/css/weather-icons.css";
import Form from './components/Form';
import Weather from './components/Weather';
const API_key="b55b61246e239f978469b3fa71d619e0";
class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      city:undefined,
      country:undefined,
      icon:undefined,
      main:undefined,
      celcius:undefined,
      temp_max:undefined,
      temp_min:undefined,
      description:"",
      error:""

    };
    //this.getWeather();
    
  }
  componentDidMount(){
    this.weatherIcon={
      Thunderstorm:"wi-thunderstorm",
      Drizzle:"wi-sleet",
      Rain:"wi-storm-showers",
      Snow:"wi-snow",
      Atmosphere:"wi-fog",
      Clear:"wi-day-sunny",
      Clouds:"wi-day-fog"
    }
  }

  convertFaraniteToCelcius(temp){
    let new_temp=Math.floor(temp-273.15);
    return new_temp
  }
  get_icon(icons, range) {
    switch (true) {
      case range >= 200 && range < 232:
        this.setState({ icon: icons.Thunderstorm });
        break;
      case range >= 300 && range <= 321:
        this.setState({ icon: icons.Drizzle });
        break;
      case range >= 500 && range <= 521:
        this.setState({ icon: icons.Rain });
        break;
      case range >= 600 && range <= 622:
        this.setState({ icon: icons.Snow });
        break;
      case range >= 701 && range <= 781:
        this.setState({ icon: icons.Atmosphere });
        break;
      case range === 800:
        this.setState({ icon: icons.Clear });
        break;
      case range >= 801 && range <= 804:
        this.setState({ icon: icons.Clouds });
        break;
      default:
        this.setState({ icon: icons.Clouds });
    }
  }
  
 getWeather= async(e) =>{
    e.preventDefault();
    const city=e.target.elements.city.value;
    const country=e.target.elements.country.value;
    
    if(city && country){
      await fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_key}`
      ).then((data)=>{
          return data.json();
      }).then((data)=>{
        console.log(data);
        this.setState({
          city:`${data.name},${data.sys.country}`,
          celcius:this.convertFaraniteToCelcius(data.main.temp),
          temp_max:this.convertFaraniteToCelcius(data.main.temp_max),
          temp_min:this.convertFaraniteToCelcius(data.main.temp_min),
          description:data.weather[0].description,
          error:""
        })
        this.get_icon(this.weatherIcon,data.weather[0].id);
      }).catch((err)=>{
          this.setState({
            error:"City not found !!"
          })
      })
    }
    else{
      this.setState({error:"Please enter City and Country name !!"});
    }
    
  };   
  render(){
      return ( 

        <div className="App">
          
      {this.state.error === "" ? 
      <div>
          <h1 className="f2 pt-4"> Weather Finder</h1>
          <p> Find out the temprature weather conditions and More!</p>
        <Form loadweather={this.getWeather} ></Form>
      </div>:
      <div>
          <div className="alert alert-danger mx-5" role="alert">{this.state.error}</div>
          <h1 className="f2 pt-4"> Weather Finder</h1>
          <p> Find out the temprature weather conditions and More!</p>
          <Form loadweather={this.getWeather} ></Form>
      </div>}
          
          <Weather 
              city={this.state.city}
              country={this.state.country}
              temp_celcius={this.state.celcius}
              temp_max={this.state.temp_max}
              temp_min={this.state.temp_min}
              description={this.state.description}
              weatherIcon={this.state.icon}
          />
       </div>
      );
  }
}


export default App;
